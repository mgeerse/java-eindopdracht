package nl.inholland.university.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import nl.inholland.university.models.*;

// Class representing the user database
public class UserDatabase {
    public ObservableList<User> userList = FXCollections.observableArrayList();
    public ObservableList<Admin> adminList = FXCollections.observableArrayList();
    public ObservableList<Student> studentList = FXCollections.observableArrayList();
    public ObservableList<Teacher> teacherList = FXCollections.observableArrayList();

    public UserDatabase() {
        fillList();
    }

    // Fill the mock database with entries
    private void fillList() {
        // Admins
        adminList.add(new Admin(0, "John", "Doe", "johndoe", "1234", new Birthdate(1, 1, 1)));

        // Teachers
        teacherList.add(new Teacher(1, "Klaas", "Vaak", "klaasvaak", "1234", new Birthdate(23, 14, 1423)));
        teacherList.add(new Teacher(2, "Pim", "Pilk", "pimpilk", "1234", new Birthdate(9, 7, 2016)));

        // Students
        studentList.add(new Student(3, "Piet", "Biem", "pietbiem", "1234", "INF-04", new Birthdate(5, 5, 1994)));
        studentList.add(new Student(4, "Jaap", "Boem", "jaapboem", "1234", "INF-03", new Birthdate(8, 3, 1992)));
        studentList.add(new Student(5, "Jos", "Vier", "josvier", "1234", "INF-03", new Birthdate(11, 12, 2004)));
        studentList.add(new Student(6, "Miek", "Liep", "miekliep", "1234", "INF-01", new Birthdate(23, 10, 2000)));
        studentList.add(new Student(7, "Mak", "Piel", "makpiel", "1234", "INF-05", new Birthdate(12, 8, 2001)));
        studentList.add(new Student(8, "Pam", "Klip", "pamklip", "1234", "INF-03", new Birthdate(4, 2, 1999)));
        studentList.add(new Student(9, "Pim", "Pilk", "pimpilk", "1234", "INF-02", new Birthdate(9, 7, 1998)));

        userList.addAll(adminList);
        userList.addAll(teacherList);
        userList.addAll(studentList);
    }

    // Find a user by Id
    public Student getById(int studentId) {
        return studentList.stream().filter(x -> x.id == studentId).findFirst().get();
    }

    // Find a user by Email
    public User getByEmail(String email) {
        return userList.stream().filter(x -> x.email.equals(email)).findFirst().get();
    }

    // Check if a user exists in the database (used for authentication)
    public boolean checkCredentials(String email, String password) {
        return userList.stream().anyMatch(x -> x.email.equals(email) && x.password.equals(password));
    }
}