package nl.inholland.university.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import nl.inholland.university.models.Grade;

import java.util.stream.Collectors;

// Class representing the user database
public class GradeDatabase {

    public ObservableList<Grade> gradeList = FXCollections.observableArrayList();

    public GradeDatabase() {
        fillList();
    }

    // Fill the mock database with entries
    private void fillList() {
        gradeList.add(new Grade(3, 1, 7.8, "Java"));
        gradeList.add(new Grade(3, 2, 7.3, "PHP"));
        gradeList.add(new Grade(3, 1, 6.8, "CSharp"));
        gradeList.add(new Grade(3, 1, 6.1, "Python"));
        gradeList.add(new Grade(4, 1, 4.2, "Java"));
        gradeList.add(new Grade(4, 2, 6.4, "PHP"));
        gradeList.add(new Grade(4, 1, 5.9, "CSharp"));
        gradeList.add(new Grade(4, 1, 5.8, "Python"));
        gradeList.add(new Grade(5, 1, 9.4, "Java"));
        gradeList.add(new Grade(5, 2, 4.3, "PHP"));
        gradeList.add(new Grade(5, 1, 6.6, "CSharp"));
        gradeList.add(new Grade(5, 1, 8.0, "Python"));
        gradeList.add(new Grade(6, 1, 6.3, "Java"));
        gradeList.add(new Grade(6, 2, 2.3, "PHP"));
        gradeList.add(new Grade(6, 1, 8.1, "CSharp"));
        gradeList.add(new Grade(6, 1, 8.4, "Python"));
        gradeList.add(new Grade(7, 1, 9.9, "Java"));
        gradeList.add(new Grade(7, 2, 8.9, "PHP"));
        gradeList.add(new Grade(7, 1, 8.7, "CSharp"));
        gradeList.add(new Grade(7, 1, 9.7, "Python"));
        gradeList.add(new Grade(8, 1, 3.1, "Java"));
        gradeList.add(new Grade(8, 2, 6.9, "PHP"));
        gradeList.add(new Grade(8, 1, 6.1, "CSharp"));
        gradeList.add(new Grade(8, 1, 5.8, "Python"));
        gradeList.add(new Grade(9, 1, 5.9, "Java"));
        gradeList.add(new Grade(9, 2, 5.1, "PHP"));
        gradeList.add(new Grade(9, 1, 5.7, "CSharp"));
        gradeList.add(new Grade(9, 1, 5.5, "Python"));
    }

    // Get all grades for a student
    public ObservableList<Grade> getGradeListByStudent(int studentId) {
        //gradeList.stream().filter(x -> x.getStudentId() == studentId).collect(Collectors.toList());

        ObservableList<Grade> grades = FXCollections.observableArrayList(gradeList.stream().filter(x -> x.getStudentId() == studentId).collect(Collectors.toList()));
        return grades;
    }

    // Get a grade for a student within a course
    public double getGrade(int studentId, String courseName) {
        return gradeList.stream().filter(x -> x.getStudentId() == studentId && x.getCourseName().toLowerCase().equals(courseName.toLowerCase())).findFirst().get().getGrade();
    }

    // Set a grade for a student within a course
    public void setStudentGrade(int studentId, String courseName, double grade) {
        gradeList.stream().filter(x -> x.getStudentId() == studentId && x.getCourseName().equals(courseName)).findFirst().get().setGrade(grade);
    }
}
