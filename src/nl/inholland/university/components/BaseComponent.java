package nl.inholland.university.components;

import javafx.scene.Parent;
import javafx.stage.Stage;

public abstract class BaseComponent extends Parent {
    protected final Stage _primaryStage;

    public BaseComponent(Stage primaryStage){
        _primaryStage = primaryStage;
    }
}
