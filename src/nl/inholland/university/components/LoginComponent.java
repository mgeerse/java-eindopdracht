package nl.inholland.university.components;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import nl.inholland.university.controllers.UserController;

public class LoginComponent extends BaseComponent {

    //Nodes
    private GridPane gridPane = new GridPane();
    private Label emailLabel = new Label("Email:");
    private Label passwordLabel = new Label("Password:");
    private TextField emailInput = new TextField();
    private TextField passwordInput = new PasswordField();
    private Button loginButton = new Button("Log in");
    private Label messageLabel = new Label();

    public LoginComponent(Stage primaryStage) {
        super(primaryStage);
        init();
        setButtonHandlers();
    }

    private void init() {
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10); // Vertical spacing between grid items
        gridPane.setHgap(8);  // Horizontal spacing between grid items

        //Add the 'nodes' to the GridPane
        GridPane.setConstraints(emailLabel, 0, 0);
        GridPane.setConstraints(emailInput, 0, 1);
        GridPane.setConstraints(passwordLabel, 0, 2);
        GridPane.setConstraints(passwordInput, 0, 3);
        GridPane.setConstraints(loginButton, 0, 4);
        GridPane.setConstraints(messageLabel, 0, 5);

        // Add all form elements to the grid
        gridPane.getChildren().addAll(emailLabel, emailInput, passwordLabel, passwordInput, loginButton, messageLabel);
        this.getChildren().add(gridPane);
    }

    // Bind button handlers to the buttons
    private void setButtonHandlers() {
        loginButton.setOnAction(event -> LoginButtonHandle(event, emailInput.getText(), passwordInput.getText()));
    }

    // Called when the login-button is clicked
    private void LoginButtonHandle (ActionEvent event, String email, String password){
        if (UserController.logUserInWithCredentials(email, password)) {
            _primaryStage.setScene(new Scene(new MainComponent(_primaryStage), 900, 600));
        } else {
            messageLabel.setText("Email and Password\ncombination wrong.");
        }
    }
}

