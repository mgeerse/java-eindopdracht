package nl.inholland.university.components;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import nl.inholland.university.controllers.UserController;
import nl.inholland.university.nodes.MainMenuButton;

public class MainComponent extends BaseComponent {

    //Nodes
    private GridPane gridPane = new GridPane();
    private MainMenuButton studentsButton = new MainMenuButton("See all Students");
    private MainMenuButton teacherButton = new MainMenuButton("See all Teachers");
    private MainMenuButton generateSingleReportButton = new MainMenuButton("Students' grades");

    public MainComponent(Stage primaryStage) {
        super(primaryStage);
        init();
    }

    // Initialize the main window
    private void init() {

        // Set padding and gaps for the gridPane
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10); // Vertical spacing between grid items
        gridPane.setHgap(8);  // Horizontal spacing between grid items

        // Based on the user authorization, different buttons will be shown
        switch (UserController.getUserAccessLevel()) {
            default:
            case ACCESS_LEVEL_BASIC:
                GridPane.setConstraints(studentsButton, 0, 0);
                GridPane.setConstraints(teacherButton, 1, 0);
                gridPane.getChildren().addAll(studentsButton, teacherButton);
                break;
            case ACCESS_LEVEL_EDITOR:
                GridPane.setConstraints(studentsButton, 0, 0);
                GridPane.setConstraints(teacherButton, 1, 0);
                GridPane.setConstraints(generateSingleReportButton, 2, 0);
                gridPane.getChildren().addAll(studentsButton, teacherButton, generateSingleReportButton);
                break;
            case ACCESS_LEVEL_ADMIN:
                GridPane.setConstraints(studentsButton, 0, 0);
                GridPane.setConstraints(teacherButton, 1, 0);
                GridPane.setConstraints(generateSingleReportButton, 0, 1);
                gridPane.getChildren().addAll(studentsButton, teacherButton, generateSingleReportButton);
                break;
        }

        // Bind handlers to the buttons
        setButtonHandlers();
        // Add the gridPane to the main window
        this.getChildren().add(gridPane);
    }

    // Bind handlers to the buttons
    private void setButtonHandlers() {
        studentsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                studentsButtonHandler(event);
            }
        });

        teacherButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                teacherButtonHandler(event);
            }
        });

        generateSingleReportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                generateSingleReportButtonHandler(event);
            }
        });
    }

    // Called when the studentList-button is clicked
    private void studentsButtonHandler(ActionEvent event) {
        System.out.println("Trying to show all students...");
        _primaryStage.setScene(new Scene(new ListStudentsComponent(_primaryStage), 750, 1000));
    }

    // Called when the teacherList-button is clicked
    private void teacherButtonHandler(ActionEvent event) {
        System.out.println("Trying to show all teachers...");
        _primaryStage.setScene(new Scene(new ListTeachersComponent(_primaryStage), 900, 1000));
    }

    // Called when the singleReport-button is clicked
    private void generateSingleReportButtonHandler(ActionEvent event) {
        System.out.println("Trying to show students and grades...");
        _primaryStage.setScene(new Scene(new ReportComponent(_primaryStage), 900, 600));
    }

    // Called when the allStudents-button is clicked
    private void generateReportsForAllStudentsButtonHandler(ActionEvent event) {
        System.out.println("Clicked a button...");
        // _primaryStage.setScene(new Scene(new MainComponent(_primaryStage), 900, 600));
    }
}

