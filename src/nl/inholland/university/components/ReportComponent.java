package nl.inholland.university.components;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import nl.inholland.university.controllers.GradeController;
import nl.inholland.university.controllers.UserController;
import nl.inholland.university.data.UserDatabase;
import nl.inholland.university.helpers.ReportHelper;
import nl.inholland.university.models.ACCESS_LEVEL;
import nl.inholland.university.models.Grade;
import nl.inholland.university.models.Student;
import nl.inholland.university.models.StudentGrade;
import nl.inholland.university.nodes.MainMenuButton;


public class ReportComponent extends BaseComponent {

    private final int TABLE_START_POSITION_Y = 0;
    private GridPane gridPane = new GridPane();
    private MainMenuButton backButton = new MainMenuButton("< back");
    private TableView gradeTableView = new TableView();
    private ObservableList<Student> studentObservableList = new UserDatabase().studentList;
    private ACCESS_LEVEL access_level = UserController.getUserAccessLevel();

    public ReportComponent(Stage primaryStage) {
        super(primaryStage);
        init();
        setButtonHandlers();
    }

    private void init() {
        // Backbutton
        gridPane.add(backButton, 2, TABLE_START_POSITION_Y);

        // Creating new grid
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10); // Vertical spacing between grid items
        gridPane.setHgap(8);  // Horizontal spacing between grid items

        // Title
        var sectionTitle = new Text("Student Results");
        sectionTitle.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));
        gridPane.add(sectionTitle, 0, TABLE_START_POSITION_Y + 1);

        // Instruction Label
        Text instructionLabel = new Text("Click on a row to edit the grades of a student.");
        instructionLabel.setFont(Font.font("Tahoma", FontWeight.LIGHT, 10));
        gridPane.add(instructionLabel, 0, TABLE_START_POSITION_Y + 2);

        //Grade overview table
        createTable();

        //Save all reports if admin access
        if (this.access_level == ACCESS_LEVEL.ACCESS_LEVEL_ADMIN) {
            createSaveAll();
        }

        // Finally, add all nodes to gridPane
        this.getChildren().add(gridPane);
    }

    // Creating the 'Save All'-button
    private void createSaveAll() {
        Button generateAllReportsButton = new Button();
        generateAllReportsButton.setText("Generate reports from all students");

        generateAllReportsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                generateAllReportsHandler(event);
            }
        });

        gridPane.getChildren().addAll(generateAllReportsButton);
    }

    // Creating the table displaying the grades
    private void createTable() {
        // Assign table column values
        TableColumn<String, StudentGrade> col_id = new TableColumn<>("Id");
        TableColumn<String, StudentGrade> col_firstName = new TableColumn<>("First Name");
        TableColumn<String, StudentGrade> col_lastName = new TableColumn<>("Last Name");
        TableColumn<String, StudentGrade> col_age = new TableColumn<>("Age");
        TableColumn<String, StudentGrade> col_group = new TableColumn<>("Group");
        TableColumn<String, StudentGrade> col_java = new TableColumn<>("Java");
        TableColumn<String, StudentGrade> col_php = new TableColumn<>("PHP");
        TableColumn<String, StudentGrade> col_csharp = new TableColumn<>("CSharp");
        TableColumn<String, StudentGrade> col_python = new TableColumn<>("Python");

        // Bind values to their cells
        col_id.setCellValueFactory(new PropertyValueFactory<>("studentId"));
        col_firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        col_lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        col_age.setCellValueFactory(new PropertyValueFactory<>("age"));
        col_group.setCellValueFactory(new PropertyValueFactory<>("group"));
        col_java.setCellValueFactory(new PropertyValueFactory<>("java"));
        col_php.setCellValueFactory(new PropertyValueFactory<>("php"));
        col_csharp.setCellValueFactory(new PropertyValueFactory<>("csharp"));
        col_python.setCellValueFactory(new PropertyValueFactory<>("python"));

        // Add the new columns to the table
        gradeTableView.getColumns().addAll(col_id, col_firstName, col_lastName, col_age, col_group, col_java, col_php, col_csharp, col_python);

        //Add the students and grades to the table
        for (Student student : studentObservableList) {
            //Retrieve grades per student
            ObservableList<Grade> gradesByStudentId = GradeController.getGradesByStudentId(student.id);

            //Filter out the list for the grade
            double javaGrade = GradeController.getGrade(student.id, "Java");
            double phpGrade = GradeController.getGrade(student.id, "PHP");
            double csharpGrade = GradeController.getGrade(student.id, "CSharp");
            double pythonGrade = GradeController.getGrade(student.id, "Python");

            //Add all items to the gradeTableView
            gradeTableView.getItems().add(
                    new StudentGrade(
                            student.getId(),
                            student.getFirstName(),
                            student.getLastName(),
                            student.getBirthDate(),
                            student.getAge(),
                            student.getGroup(),
                            javaGrade,
                            phpGrade,
                            csharpGrade,
                            pythonGrade
                    ));
        }


        // Show a placeholder if no items can be found
        gradeTableView.setPlaceholder(new Label("No rows to display"));

        // Set resizing policy for the tableview
        gradeTableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);

        // Bind the observable list to the tableview
        gradeTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            //newSelection should not be null, should be StudentGrade
            if (newSelection != null) {
                StudentGrade studentGrade = (StudentGrade) newSelection;
                Stage stage = new Stage();
                Scene scene = new Scene(new EditStudentResultsComponent(_primaryStage, stage, studentGrade), 600, 400);
                stage.setTitle("Editing the results of: " + studentGrade.getFirstName() + " " + studentGrade.getLastName());
                stage.setScene(scene);
                _primaryStage.hide();
                stage.show();
            }
        });

        // Add the table to the grid pane
        gridPane.add(gradeTableView, 0, TABLE_START_POSITION_Y + 3);
    }

    // Bind buttons to their handlers
    private void setButtonHandlers() {
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backButtonHandler(event);
            }
        });
    }

    // Called when the back-button is clicked
    private void backButtonHandler(ActionEvent event) {
        System.out.println("Going back...");
        _primaryStage.setScene(new Scene(new MainComponent(_primaryStage), 900, 600));
    }

    // Called when the generateAllReports-button is clicked
    private void generateAllReportsHandler(ActionEvent event) {
        System.out.println("Generating reports for all the students...");
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Saving all reports");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to save all reports?");
        alert.initOwner(gridPane.getScene().getWindow());

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                ReportHelper reportHelper = new ReportHelper();
                reportHelper.generateReports();

                showAlert(Alert.AlertType.INFORMATION, gridPane.getScene().getWindow(),
                        "Reports saved", "The reports have been successfully saved.");
            }
        });
    }

    // Creates an alert window
    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
}
