package nl.inholland.university.components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import nl.inholland.university.controllers.UserController;
import nl.inholland.university.models.Birthdate;
import nl.inholland.university.models.Student;
import nl.inholland.university.models.User;
import nl.inholland.university.nodes.MainMenuButton;

import java.util.Collections;
import java.util.Comparator;

public class ListStudentsComponent extends BaseComponent {

    private final int TABLE_START_POSITION_Y = 0;
    private final int FORM_START_POSITION_Y = 0;

    GridPane gridPane = new GridPane();

    MainMenuButton backButton = new MainMenuButton("< back");

    TableView studentTableView = new TableView();
    ObservableList<Student> studentObservableList = UserController.getStudentList();
    ObservableList<User> userObservableList = UserController.getUserList();

    public ListStudentsComponent(Stage primaryStage) {
        super(primaryStage);
        init();
        setButtonHandlers();
    }

    private void init() {
        // Creating new grid
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10); // Vertical spacing between grid items
        gridPane.setHgap(8);  // Horizontal spacing between grid items

        // Bind handlers to buttons
        setButtonHandlers();
        // Add the back button to the grid
        gridPane.add(backButton,TABLE_START_POSITION_Y,0);

        var sectionTitle = new Text("All students");
        sectionTitle.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));
        // Add the title to the grid pane
        gridPane.add(sectionTitle, 0,TABLE_START_POSITION_Y+1);

        // Construct student table
        createTable();
        switch (UserController.getUserAccessLevel()){
            case ACCESS_LEVEL_EDITOR:
            case ACCESS_LEVEL_ADMIN:
                createForm();
                break;
        }

        // Add all objects to the grid
        this.getChildren().add(gridPane);
    }

    // Bind handlers to their buttons
    private void setButtonHandlers(){
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                backButtonHandler(event);
            }
        });
    }

    private void createTable()
    {
        // Assign table column values
        TableColumn<String, Student> col_id = new TableColumn<>("Id");
        TableColumn<String, Student> col_firstName = new TableColumn<>("First Name");
        TableColumn<String, Student> col_lastName = new TableColumn<>("Last Name");
        TableColumn<String, Student> col_age = new TableColumn<>("Age");
        TableColumn<String, Student> col_group = new TableColumn<>("Group");

        // Bind values to their cells
        col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        col_firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        col_lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        col_age.setCellValueFactory(new PropertyValueFactory<>("age"));
        col_group.setCellValueFactory(new PropertyValueFactory<>("group"));

        // Add the new columns to the table
        studentTableView.getColumns().addAll(col_id, col_firstName, col_lastName, /*col_birthDate,*/ col_age, col_group);
        // Add all students to the table
        studentTableView.getItems().addAll(studentObservableList);
        // Show a placeholder if no items can be found
        studentTableView.setPlaceholder(new Label("No rows to display"));
        // Set resize policy for the tableview
        studentTableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        // Add the table to the grid pane
        gridPane.add(studentTableView,0,TABLE_START_POSITION_Y+2);
    }

    // Creates the from which is used to create new students
    private void createForm()
    {
        var sectionTitleLabel = new Text("Create New Student");
        sectionTitleLabel.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));
        // Add the title to the grid pane
        gridPane.add(sectionTitleLabel, 0,FORM_START_POSITION_Y+3);

        /// Constructing form for creating new student
        // FIRST NAME
        Label firstNameLabel = new Label("First Name:");
        gridPane.add(firstNameLabel, 0, FORM_START_POSITION_Y+4);
        TextField firstNameTextField = new TextField();
        gridPane.add(firstNameTextField, 0, FORM_START_POSITION_Y+5);

        // LAST NAME
        Label lastNameLabel = new Label("Last Name:");
        gridPane.add(lastNameLabel, 0, FORM_START_POSITION_Y+6);
        TextField lastNameTextField = new TextField();
        gridPane.add(lastNameTextField, 0, FORM_START_POSITION_Y+7);

        // DAY
        Label dayLabel = new Label("Birth Date Day");
        gridPane.add(dayLabel, 0, FORM_START_POSITION_Y+8);
        TextField dayTextField = new TextField();
        dayTextField.setPrefWidth(80);
        dayTextField.setMaxWidth(80);
        gridPane.add(dayTextField, 0, FORM_START_POSITION_Y+9);

        // MONTH
        Label monthLabel = new Label("Birth Date Month");
        gridPane.add(monthLabel, 0, FORM_START_POSITION_Y+10);
        TextField monthTextField = new TextField();
        gridPane.add(monthTextField, 0, FORM_START_POSITION_Y+11);

        // YEAR
        Label yearLabel = new Label("Birth Date Year");
        gridPane.add(yearLabel, 0, FORM_START_POSITION_Y+12);
        TextField yearTextField = new TextField();
        gridPane.add(yearTextField, 0, FORM_START_POSITION_Y+13);

        // GROUP
        Label groupLabel = new Label("Group:");
        gridPane.add(groupLabel, 0, FORM_START_POSITION_Y+14);
        TextField groupTextField = new TextField();
        gridPane.add(groupTextField, 0, FORM_START_POSITION_Y+15);

        // PASSWORD
        Label passwordLabel = new Label("Password:");
        gridPane.add(passwordLabel, 0, FORM_START_POSITION_Y+16);
        PasswordField passwordTextField = new PasswordField();
        gridPane.add(passwordTextField, 0, FORM_START_POSITION_Y+17);

        // SUBMIT BUTTON
        MainMenuButton submitFormButton = new MainMenuButton("Add student");
        gridPane.add(submitFormButton, 0, FORM_START_POSITION_Y+18);

        // SUBMIT BUTTON HANDLER (Check if any fields are not filled in)
        submitFormButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            // Data validation on the new student form
            public void handle(ActionEvent event) {
                if(firstNameTextField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter the first name");
                    return;
                }
                if(lastNameTextField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter the last name");
                    return;
                }
                if(dayTextField.getText().isEmpty() || monthTextField.getText().isEmpty() || yearTextField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter all birth date fields");
                    return;
                }
                if(groupTextField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter the group");
                    return;
                }
                if(passwordTextField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                            "Form Error!", "Please enter the password");
                    return;
                }

                // Grab the last ID from all users by sorting the list and grabbing the last
                Collections.sort(userObservableList, Comparator.comparing(User::getId));
                // Add one to create a unique ID
                var lastUserId = userObservableList.get(userObservableList.size() - 1).id;

                // Construct new birthDate object from int values
                var newBirthDate = new Birthdate();
                newBirthDate.setDay(Integer.parseInt(dayTextField.getText()));
                newBirthDate.setMonth(Integer.parseInt(monthTextField.getText()));
                newBirthDate.setYear(Integer.parseInt(yearTextField.getText()));

                // Construct new student
                var newStudent = new Student();
                newStudent.setId(lastUserId+1);
                newStudent.setFirstName(firstNameTextField.getText());
                newStudent.setLastName(lastNameTextField.getText());
                newStudent.setEmail(firstNameTextField.getText() + "." + lastNameTextField.getText() + "@inholland.student.nl");
                newStudent.setGroup(groupTextField.getText());
                newStudent.setBirthDate(newBirthDate);
                newStudent.setPassword(passwordTextField.getText());

                // Add student to list
                studentObservableList.add(newStudent);
                userObservableList.add(newStudent);

                // Refresh
                var newStudentObservableList = studentObservableList;
                studentTableView.getItems().clear();
                studentTableView.getItems().addAll(FXCollections.observableArrayList(newStudentObservableList));

                // Clear the fields
                firstNameTextField.clear();
                lastNameTextField.clear();
                groupTextField.clear();
                dayTextField.clear();
                monthTextField.clear();
                yearTextField.clear();
                passwordTextField.clear();
            }
        });
    }

    // Called when the back-button is clicked
    private void backButtonHandler(ActionEvent event){
        System.out.println("Going back...");
        _primaryStage.setScene(new Scene(new MainComponent(_primaryStage), 900, 600));
    }

    // Creates an alert
    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
}
