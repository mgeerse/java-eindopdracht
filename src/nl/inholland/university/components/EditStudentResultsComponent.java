package nl.inholland.university.components;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import nl.inholland.university.controllers.GradeController;
import nl.inholland.university.helpers.ReportHelper;
import nl.inholland.university.models.StudentGrade;
import nl.inholland.university.nodes.MainMenuButton;

import java.text.DecimalFormat;

public class EditStudentResultsComponent extends BaseComponent {

    private final int FORM_START_POSITION_Y = 0;
    MainMenuButton backButton = new MainMenuButton("< back");
    MainMenuButton generateReportButton = new MainMenuButton("Generate a report of this student");
    GridPane gridPane = new GridPane();
    private StudentGrade studentGrade;
    private Stage thisStage;

    public EditStudentResultsComponent(Stage primaryStage, Stage thisStage, StudentGrade studentGrade) {
        super(primaryStage);
        this.studentGrade = studentGrade;
        this.thisStage = thisStage;
        init();
    }

    private void init() {
        // Creating new grid
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10); // Vertical spacing between grid items
        gridPane.setHgap(8);  // Horizontal spacing between grid items

        // Bind handlers to buttons
        setButtonHandlers();
        // Add the back button to the grid
        gridPane.add(backButton, FORM_START_POSITION_Y, 0);
        gridPane.add(generateReportButton, FORM_START_POSITION_Y + 1, 0);
        createForm();

        this.getChildren().add(gridPane);
    }

    // Creates the form which is used to modify the grades
    private void createForm() {
        var sectionTitleLabel = new Text("Modify the grades");
        sectionTitleLabel.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));

        // Java
        Label javaLabel = new Label("Java grade:");
        TextField javaTextField = new TextField();
        javaTextField.setText(String.valueOf(studentGrade.getJava()));

        // CSharp
        Label csharpLabel = new Label("CSharp grade:");
        TextField csharpTextField = new TextField();
        csharpTextField.setText(String.valueOf(studentGrade.getCsharp()));

        // PHP
        Label phpLabel = new Label("PHP grade:");
        TextField phpTextField = new TextField();
        phpTextField.setText(String.valueOf(studentGrade.getPhp()));

        // Python
        Label pythonLabel = new Label("Python grade:");
        TextField pythonTextField = new TextField();
        pythonTextField.setText(String.valueOf(studentGrade.getPython()));

        // SUBMIT BUTTON
        MainMenuButton submitFormButton = new MainMenuButton("Update grades");

        gridPane.add(javaLabel, 0, FORM_START_POSITION_Y + 4);
        gridPane.add(javaTextField, 0, FORM_START_POSITION_Y + 5);
        gridPane.add(csharpLabel, 0, FORM_START_POSITION_Y + 6);
        gridPane.add(csharpTextField, 0, FORM_START_POSITION_Y + 7);
        gridPane.add(phpLabel, 0, FORM_START_POSITION_Y + 8);
        gridPane.add(phpTextField, 0, FORM_START_POSITION_Y + 9);
        gridPane.add(pythonLabel, 0, FORM_START_POSITION_Y + 10);
        gridPane.add(pythonTextField, 0, FORM_START_POSITION_Y + 11);
        gridPane.add(submitFormButton, 0, FORM_START_POSITION_Y + 12);

        // SUBMIT BUTTON HANDLER (Check if any fields are invalid)
        submitFormButton.setOnAction(event -> {
            //There's no good way to check if the input is a double.
            //The only method available is Double.parseDouble but this throws an error when it's not parsable, so we have to make a try catch
            try {
                DecimalFormat decimalFormat = new DecimalFormat("#.#");
                double javaGrade = Double.parseDouble(javaTextField.getText());
                double phpGrade = Double.parseDouble(phpTextField.getText());
                double csharpGrade = Double.parseDouble(csharpTextField.getText());
                double pythonGrade = Double.parseDouble(pythonTextField.getText());

                if (javaGrade != studentGrade.getJava()) {
                    GradeController.setStudentGrade(studentGrade.getStudentId(), "Java", javaGrade);
                }
                if (phpGrade != studentGrade.getPhp()) {
                    GradeController.setStudentGrade(studentGrade.getStudentId(), "PHP", phpGrade);
                }
                if (csharpGrade != studentGrade.getCsharp()) {
                    GradeController.setStudentGrade(studentGrade.getStudentId(), "CSharp", csharpGrade);
                }
                if (pythonGrade != studentGrade.getPython()) {
                    GradeController.setStudentGrade(studentGrade.getStudentId(), "Python", pythonGrade);
                }
            } catch (Exception e) {
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(),
                        "Form Error!", "One of the grades isn't a valid grade.");
            }
        });
    }

    // Bind buttons to their handlers
    private void setButtonHandlers() {
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backButtonHandler(event);
            }
        });

        generateReportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                generateReportButtonHandler(event);
            }
        });
    }

    // Called when the back-button is clicked
    private void backButtonHandler(ActionEvent event) {
        System.out.println("Going back to ReportComponent...");
        _primaryStage.setScene(new Scene(new ReportComponent(_primaryStage), 900, 600));
        _primaryStage.show();
        thisStage.close();
    }

    // Called when the generateReport-button is clicked
    private void generateReportButtonHandler(ActionEvent event) {
        ReportHelper reportHelper = new ReportHelper();
        reportHelper.generateReportById(studentGrade.getStudentId());

        showAlert(Alert.AlertType.INFORMATION, gridPane.getScene().getWindow(),
                "Report saved", "The report for this student has been successfully saved.");
    }

    // Creates an alert
    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
}
