package nl.inholland.university.components;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import nl.inholland.university.data.UserDatabase;
import nl.inholland.university.models.Student;
import nl.inholland.university.models.Teacher;
import nl.inholland.university.nodes.MainMenuButton;

import java.util.List;

public class ListTeachersComponent extends BaseComponent {

    GridPane gridPane = new GridPane();
    MainMenuButton backButton = new MainMenuButton("Back");
    TableView teacherTableView = new TableView();
    List<Teacher> teachersList = new UserDatabase().teacherList;

    int TABLE_START_POSITION_Y = 0;

    public ListTeachersComponent(Stage primaryStage) {
        super(primaryStage);
        init();
        setButtonHandlers();
    }

    private void init()
    {
        // Creating new grid
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10); // Vertical spacing between grid items
        gridPane.setHgap(8);  // Horizontal spacing between grid items

        // Bind handlers to buttons
        setButtonHandlers();
        // Add the back button to the grid
        gridPane.add(backButton,TABLE_START_POSITION_Y,0);

        var sectionTitle = new Text("All teachers");
        sectionTitle.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));
        // Add the title to the grid pane
        gridPane.add(sectionTitle, 0,TABLE_START_POSITION_Y+1);

        // Construct student table
        createTable();

        // Add all objects to the grid
        this.getChildren().add(gridPane);
    }

    // Bind buttons to their handlers
    private void setButtonHandlers(){
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                backButtonHandler(event);
            }
        });
    }

    // Called when the back-button is clicked
    private void backButtonHandler(ActionEvent event){
        System.out.println("Going back...");
        _primaryStage.setScene(new Scene(new MainComponent(_primaryStage), 900, 600));
    }

    // Creates the table that lists the teachers
    private void createTable()
    {
        // Assign table column values
        TableColumn<String, Student> col_id = new TableColumn<>("Id");
        TableColumn<String, Student> col_firstName = new TableColumn<>("First Name");
        TableColumn<String, Student> col_lastName = new TableColumn<>("Last Name");
        TableColumn<String, Student> col_age = new TableColumn<>("Age");

        // Bind values to their cells
        col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        col_firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        col_lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        col_age.setCellValueFactory(new PropertyValueFactory<>("age"));

        // Add the new columns to the table
        teacherTableView.getColumns().addAll(col_id, col_firstName, col_lastName, /*col_birthDate,*/ col_age);
        // Add all students to the table
        teacherTableView.getItems().addAll(teachersList);
        // Show a placeholder if no items can be found
        teacherTableView.setPlaceholder(new Label("No rows to display"));

        teacherTableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);

        // Add the table to the grid pane
        gridPane.add(teacherTableView,0,TABLE_START_POSITION_Y+2);
    }
}
