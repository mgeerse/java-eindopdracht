package nl.inholland.university.nodes;

import javafx.scene.control.Button;

public class MainMenuButton extends Button {
    public MainMenuButton(String text) {
        super(text);
        setWidth(50.0);
        setHeight(50.0);
    }
}


