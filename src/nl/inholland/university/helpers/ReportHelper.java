package nl.inholland.university.helpers;

import javafx.collections.ObservableList;
import nl.inholland.university.controllers.GradeController;
import nl.inholland.university.controllers.UserController;
import nl.inholland.university.models.Grade;
import nl.inholland.university.models.Student;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class ReportHelper {

    private final String FILE_PATH = "./reports/";
    private final String FILE_EXTENSION = ".txt";

    public ReportHelper() {

    }

    public void generateReportById(int studentId) {
        ObservableList<Grade> gradesList = GradeController.getGradesByStudentId(studentId);
        Student student = UserController.getStudent(studentId);

        String reportText = generateReportText(student, gradesList);
        String fileName = String.format("%s %s %s", student.getId(), student.getFirstName(), student.getLastName());
        saveToFile(reportText, fileName);
    }

    public void generateReports() {
        for (Student student : UserController.getStudentList()) {
            ObservableList<Grade> gradesList = GradeController.getGradesByStudentId(student.getId());
            String reportText = generateReportText(student, gradesList);
            String fileName = String.format("%s %s %s", student.getId(), student.getFirstName(), student.getLastName());
            saveToFile(reportText, fileName);
        }
    }

    public String generateReportText(Student student, ObservableList<Grade> grades) {
        StringBuilder stringBuilder = new StringBuilder();
        //Introduction
        stringBuilder.append(String.format("Report of student %s %s", student.getFirstName(), student.getLastName())).append(System.getProperty("line.separator"));

        stringBuilder.append(System.getProperty("line.separator"));

        //Student information
        stringBuilder.append("PERSONAL DETAILS").append(System.getProperty("line.separator"));
        stringBuilder.append(String.format("Student Id: %s", student.getId())).append(System.getProperty("line.separator"));
        stringBuilder.append(String.format("First name: %s", student.getFirstName())).append(System.getProperty("line.separator"));
        stringBuilder.append(String.format("Last name: %s", student.getLastName())).append(System.getProperty("line.separator"));
        stringBuilder.append(String.format("Birthday: %s", student.getBirthDate().toString())).append(System.getProperty("line.separator"));
        stringBuilder.append(String.format("Age: %s", student.getAge())).append(System.getProperty("line.separator"));
        stringBuilder.append(String.format("Group: %s", student.getGroup())).append(System.getProperty("line.separator"));

        //Some new lines
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append(System.getProperty("line.separator"));

        //Grades
        stringBuilder.append("COURSES").append(System.getProperty("line.separator"));
        for (Grade grade : grades) {
            stringBuilder.append(String.format("%s: %s", grade.getCourseName(), grade.getGrade())).append(System.getProperty("line.separator"));
        }

        return stringBuilder.toString();
    }

    public void saveToFile(String text, String fileName) {
//        fileName = FILE_PATH + fileName + FILE_EXTENSION;
        fileName = fileName + FILE_EXTENSION;
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(fileName), StandardCharsets.UTF_8))) {
            writer.write(text);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
