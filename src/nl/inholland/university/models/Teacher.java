package nl.inholland.university.models;

import static nl.inholland.university.models.ACCESS_LEVEL.*;

// Teacher (inherited of User) model with a set authorization level of 'ACCESS_LEVEL_EDITOR'
public class Teacher extends User
{
    public Teacher(int id, String firstName, String lastName, String email, String password, Birthdate birthdate) {
        super(id, ACCESS_LEVEL_EDITOR, firstName, lastName, email, password, birthdate);
    }
}