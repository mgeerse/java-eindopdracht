package nl.inholland.university.models;

import static nl.inholland.university.models.ACCESS_LEVEL.*;

// Student (inherited of User) model with a set authorization level of 'ACCESS_LEVEL_BASIC' & a group property
public class Student extends User {
    String group;

    public Student()
    {
        super();
    }

    public Student(
            int id,
            String firstName,
            String lastName,
            String email,
            String password,
            String group,
            Birthdate birthdate)
    {
        super(id, ACCESS_LEVEL_BASIC, firstName, lastName, email, password, birthdate);
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}