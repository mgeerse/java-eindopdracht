package nl.inholland.university.models;

import static nl.inholland.university.models.ACCESS_LEVEL.*;

// Admin (inherited of User) model with a set authorization level of 'ACCESS_LEVEL_ADMIN'
public class Admin extends User {

    public Admin(int id, String firstName, String lastName, String email, String password, Birthdate birthdate) {
        super(id, ACCESS_LEVEL_ADMIN, firstName, lastName, email, password, birthdate);
    }
}