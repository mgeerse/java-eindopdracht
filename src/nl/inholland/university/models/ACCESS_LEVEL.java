package nl.inholland.university.models;

// Enumerator model for the authorization control within the application
public enum ACCESS_LEVEL {
    ACCESS_LEVEL_BASIC, ACCESS_LEVEL_EDITOR, ACCESS_LEVEL_ADMIN
}
