package nl.inholland.university.models;

import java.text.SimpleDateFormat;

// Model to map and control the birth date of the users
public class Birthdate {
    int day;
    int month;
    int year;

    public Birthdate() { }

    public Birthdate(int Day, int Month, int Year) {
        day = Day;
        month = Month;
        year = Year;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    // Return a value in the following format
    public String toString() {
        return String.format("%d/%d/%d", day, month, year);
    }
}