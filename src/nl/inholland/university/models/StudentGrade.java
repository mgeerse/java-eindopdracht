package nl.inholland.university.models;

//This class is used when you couple students to their grade
public class StudentGrade {

    private int studentId;
    private String firstName;
    private String lastName;
    private Birthdate birthdate;
    private int age;
    private String group;
    private double java;
    private double csharp;
    private double php;
    private double python;

    public StudentGrade(int studentId, String firstName, String lastName, Birthdate birthdate, int age, String group, double java, double csharp, double php, double python) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.age = age;
        this.group = group;
        this.java = java;
        this.csharp = csharp;
        this.php = php;
        this.python = python;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Birthdate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Birthdate birthdate) {
        this.birthdate = birthdate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public double getJava() {
        return java;
    }

    public void setJava(double java) {
        this.java = java;
    }

    public double getCsharp() {
        return csharp;
    }

    public void setCsharp(double csharp) {
        this.csharp = csharp;
    }

    public double getPhp() {
        return php;
    }

    public void setPhp(double php) {
        this.php = php;
    }

    public double getPython() {
        return python;
    }

    public void setPython(double python) {
        this.python = python;
    }
}
