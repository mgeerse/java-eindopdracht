package nl.inholland.university.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

// Base class for all users with standard properties
public class User
{
    public int id;
    public ACCESS_LEVEL ACCESS_LEVEL;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public Birthdate birthDate;
    public int age;

    public User(int id, ACCESS_LEVEL ACCESS_LEVEL, String firstName, String lastName, String email, String password, Birthdate birthDate) {
        this.id = id;
        this.ACCESS_LEVEL = ACCESS_LEVEL;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.age = CalculateAge(birthDate.day, birthDate.month, birthDate.year);
    }

    public User() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Birthdate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Birthdate birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(Birthdate birthDate) {
        this.age = CalculateAge(birthDate.day, birthDate.month, birthDate.year);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.birthDate = birthDate;
    }

    private int CalculateAge(int day, int month, int year)
    {
        int result;

        // New calendar instance
        Calendar calendar = Calendar.getInstance();

        // Get current date
        int currentDay = calendar.get(Calendar.DATE);
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentYear = calendar.get(Calendar.YEAR);

        // Calculate the differences in birth date and current date
        int yearDiff = currentYear - year;
        int monthDiff = currentMonth - month;
        int dayDiff = currentDay - day;

        // Calculate the age based on birth date and current date
        if(yearDiff > 0)
        {
            // If there is a positive value, the birth date is still in the future
            if(monthDiff <= 0)
            {
                // If there is a positive value, the birth date is still in the future
                if(dayDiff <= 0)
                {
                    result = yearDiff;
                }
                else
                {
                    result = yearDiff-1;
                }
            }
            else
            {
                result = yearDiff-1;
            }
        }
        else
        {
            // Birth date is set after the current date
            result = -1;
        }

        return result;
    }
}