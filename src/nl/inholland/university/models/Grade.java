package nl.inholland.university.models;

// Grade model which represents the grade of users
public class Grade {
    private int studentId;
    private int teacherId;
    private double grade;
    private String courseName;

    public Grade(int studentId, int teacherId, double grade, String courseName) {
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.grade = grade;
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
}

