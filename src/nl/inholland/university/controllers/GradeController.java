package nl.inholland.university.controllers;

import javafx.collections.ObservableList;
import nl.inholland.university.data.GradeDatabase;
import nl.inholland.university.models.Grade;

public class GradeController {

    private static final GradeDatabase GRADE_DATABASE = new GradeDatabase();

    public static ObservableList<Grade> getGrades() {
        return GRADE_DATABASE.gradeList;
    }

    public static ObservableList<Grade> getGradesByStudentId(int studentId) {
        return GRADE_DATABASE.getGradeListByStudent(studentId);
    }

    public static void setStudentGrade(int studentId, String courseName, double grade) {
        GRADE_DATABASE.setStudentGrade(studentId, courseName, grade);
    }

    public static double getGrade(int studentId, String courseName) {
        return GRADE_DATABASE.getGrade(studentId, courseName);
    }
}

