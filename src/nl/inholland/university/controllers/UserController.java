package nl.inholland.university.controllers;

import javafx.collections.ObservableList;
import nl.inholland.university.data.UserDatabase;
import nl.inholland.university.models.*;

public class UserController {

    private static User _loggedInUser;
    private static final UserDatabase USER_DATABASE = new UserDatabase();

    // Returns true if username and password combination is found, else false.
    // If result is true, the user in this class gets set.
    public static boolean logUserInWithCredentials(String email, String password) {
        if (USER_DATABASE.checkCredentials(email, password)) {
            _loggedInUser = USER_DATABASE.getByEmail(email);
            return true;
        } else {
            return false;
        }
    }

    public static void setLoggedInUser(User loggedInUser) {
        _loggedInUser = loggedInUser;
    }

    public static User getLoggedInUser() throws NullPointerException { return _loggedInUser; }

    public static Student getStudent(int studentId) {
        return USER_DATABASE.getById(studentId);
    }

    public static ObservableList<Student> getStudentList() {
        return USER_DATABASE.studentList;
    }

    public static ObservableList<Admin> getAdminList() {
        return USER_DATABASE.adminList;
    }

    public static ObservableList<Teacher> getTeacherList() {
        return USER_DATABASE.teacherList;
    }

    public static ObservableList<User> getUserList() {
        return USER_DATABASE.userList;
    }

    public static ACCESS_LEVEL getUserAccessLevel() {
        return _loggedInUser.ACCESS_LEVEL;
    }
}
