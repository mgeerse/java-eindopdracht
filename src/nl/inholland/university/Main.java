package nl.inholland.university;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import nl.inholland.university.components.LoginComponent;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("The University Project");

        // Make our first component
        LoginComponent loginComponent = new LoginComponent(primaryStage);

        // Create a scene with our loginComponent
        Scene LoginScene = new Scene(loginComponent, 200, 300);
        primaryStage.setScene(LoginScene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
