# Preface
This assignment needs to be completed as preparation for the examination of Java Fundamentals. 
Before the examination this assignment is submitted via Moodle. During the examination you will be asked to add some functionality to your end assignment. 

# The university project
Hopefully you have all worked on the university assignment, and you will have your base classes ready –Person,Student, Teacher, etc. 
Also, there’s some authentication and authorization in place.

# Assignment
Make a JavaFX application, that will have the same basic functionality as the university project. 
Refer to the university project PDF on Moodle for this basic functionality. 
You will have:
1. [x] A login screen with real authorization. This means you will have a mechanism in place to match a username to a password.The emailaddress is the usernamefor all entities–students, teachers and managers.
1. [x] According to the access level (basic, editor, admin) users will have access to different functionality.
1. [x] User scan add, list and edit details of persons, as well as enter grades, depending on their access level.
1. [x] Users will have the ability to generate reports as specified in the university project PDF. 

## Important criteria
To successfully complete this assignment, keep the following criteria in mind:
1. [ ]  Think Object Oriented.
2. [ ]  Use Data Hiding (Encapsulation) and Java Bean Definitions.
3. [ ]  Spread your logic over the classes in a sensible way.
4. [ ]  Don’t use the default package, but let your “main”class reside in a package, e.g. nl.inholland.
5. [ ]  Don’t use static variables, other than constants. Use static methods only as part of a design pattern, e.g. the Factory Pattern. You will not need the Singleton pattern, and is prohibited.
6. [ ]  Use the proper formatting of your code. Indent in a proper way and add blank lines where appropriate.
7. [ ]  Give classes, variables, and methods good names. Code must be well readable.
8. [ ]  Non-working applications will get no points. Applications that take more than 10 minutes of debugging for it to work on my machine will be considered to be not working.
9. [ ]  Most of what you need –if not all –to complete this assignment you can find in the slides. If you use StackOverflow or any other website, and you get confused, return to the slides. If you have researched your problem thoroughly, and you still don’t have a solution, submit your code on Github (or equivalent) and ask me to review a specific question.
10. [ ]  Use AdoptOpenJDK11. If you are using a higher version, e.g. 12 or 13 (14 has just been released) it will not work on my machine. You can export your code to a lower level if you are using a higher level.
11. [ ]  Submit your code as ajar file that includes sourcecode! If your jar file does not contain sourcecode, you will get no points. It does not need to be runnable. I will not decompile your code to make it readable, because it will not have the proper naming in place.
12. [ ]  For the assignment a maximum of 45 points can be attained. For the exam a maximum of 45 points can be attained.
13. [ ]  It’s not possible to successfully complete this course by only submitting the end assignment without doing the actual exam


# Data from slides
The system allows users mainly students, teachers and managers of the university to access specific features of the application. To avoid spending time on designing relational databases, connections and using database tools, a Java class is used for all the test data.

## Functional design
1. [x] The access to the application requires an authentication.
2. [x] By successful login, the system searches for the user. If the user is found, the system checks his/her access level and accordingly the options appear.
3. [x] There are three levels of access namely:Basic, Editor and Admin.
4. [x] By default,students access level is basic, teachers access is editor and managers are admins. 

### Student (basic access level)
Students can access:
1. [x] Display the list of the students 
2. [x] Display the list of the teachers 

### Teacher (editor access level)
Teachers can access all the functionalities that are available for the students with the following extra options.
1. [ ] Add Students 
2. [x] Display Reports 
3. [x] Display Report Details 
4. [x] Add and Modify Reports

### Managers (admin access level)
Managers by default can access all the functionalities that are available for the students and the teachers. 
Besides they can generate reports of all the students.

They also need to be able to:
1. [x] Save Reports, this needs to be stored in a document (can be plaintext)
